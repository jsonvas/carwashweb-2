import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const state = {
    data: [],
    carrito: []
}

const mutations = {
    setItem(state, id) {
        let item;
        let a = 0;
        for (let prod of state.data) {
            if (prod.id == id) {
                item = {
                    producto: prod,
                    cantidad: 1
                };
            }
        }

        a = !state.carrito.some(it => it.producto.id == id);

        a? state.carrito.push(item):console.log("Item ya seleccionado.")
        console.log(a);
    },
    setData(state, data) {
        state.data = data;
    },
    deleteItem(state, id) {
        for (let i of state.carrito) {
            if (i.producto.id == id) {
                state.carrito.splice(state.carrito.indexOf(i), 1);
            }
        }
    }
}

const actions = {
    getItems: async function ({ commit }) {
        const data = await axios.get('http://localhost:8082/productos/producto/');
        commit('setData', data.data)
    }
}

const vuex = new Vuex.Store({
    state,
    mutations,
    actions
})

export default vuex;