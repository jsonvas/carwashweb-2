import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/auth/Login'
import Home from '@/components/Home'
import Productos from '@/components/producto/Productos'
import Proveedores from '@/components/proveedor/Proveedores'
import Clientes from '@/components/cliente/Clientes'
import NuevoCliente from '@/components/cliente/NuevoCliente'
import NuevoProducto from '@/components/producto/NuevoProducto'
import Compras from '@/components/compra/Compras'
import Ventas from '@/components/venta/Ventas'
import NuevaVenta from '@/components/venta/NuevaVenta'
import NuevaCompra from '@/components/compra/NuevaCompra'
import NuevoProveedor from '@/components/proveedor/NuevoProveedor'
import Empresas from '@/components/cliente/Empresas'
import LavaderoServicios from '@/components/servicio/LavaderoServicios'
import ExternoServicios from '@/components/servicio/ExternoServicios'


Vue.use(Router)

const routes = [
  
  {
    path: '/login',
    name: 'Login',
    component: Login,
    meta: { requiresAuth: false }
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { requiresAuth: true }
  },
  {
    path: '/servicios/lavadero',
    name: 'LavaderoServicios',
    component: LavaderoServicios,
    meta: { requiresAuth: true }
  },
  {
    path: '/compras/nuevo',
    name: 'NuevaCompra',
    component: NuevaCompra,
    meta: { requiresAuth: true }
  },
  {
    path: '/ventas',
    name: 'Ventas',
    component: Ventas,
    meta: { requiresAuth: true }
  },
  {
    path: '/ventas/nuevo',
    name: 'NuevaVenta',
    component: NuevaVenta,
    meta: { requiresAuth: true }
  },
  {
    path: '/servicios/externo',
    name: 'ExternoServicios',
    component: ExternoServicios,
    meta: { requiresAuth: true }
  },
  {
    path: '/productos',
    name: 'Productos',
    component: Productos,
    meta: { requiresAuth: true }
  },
  {
    path: '/compras',
    name: 'Compras',
    component: Compras,
    meta: { requiresAuth: true }
  },
  {
    path: '/proveedores',
    name: 'Proveedores',
    component: Proveedores,
    meta: { requiresAuth: true }
  },
  {
    path: '/proveedores/nuevo',
    name: 'NuevoProveedor',
    component: NuevoProveedor,
    meta: { requiresAuth: true }
  },
  {
    path: '/empresas',
    name: 'Empresas',
    component: Empresas,
    meta: { requiresAuth: true }
  },
  {
    path: '/clientes',
    name: 'Clientes',
    component: Clientes,
    meta: { requiresAuth: true }
  },
  {
    path: '/clientes/nuevo',
    name: 'NuevoCliente',
    component: NuevoCliente,
    meta: { requiresAuth: true }
  },
  {
    path: "/productos/nuevo",
    name: "NuevoProducto",
    component: NuevoProducto,
    meta: { requiresAuth: true }
  }
]

const router =  new Router({
  mode:'history',
  routes
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)){
    const user = localStorage.getItem('user');
    if(user == null){
      console.log('Acceso no Autorizado')
      next('/login');
    }else{
      next();
    }
  }{
    next();
  }
  
});

export default router;